import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ProduitService } from './service/produit.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProduitFormComponent } from './produit-form/produit-form.component';
import { ProduitTableComponent } from './produit-table/produit-table.component';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ProduitFormComponent,
    ProduitTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatDialogModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    ProduitService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
