import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProduitService } from '../service/produit.service';
import { MatDialog } from '@angular/material/dialog';
import { ProduitFormComponent } from '../produit-form/produit-form.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-produit-table',
  templateUrl: './produit-table.component.html',
  styleUrls: ['./produit-table.component.css']
})
export class ProduitTableComponent implements OnInit{

  constructor(private produitService: ProduitService, private matDialog: MatDialog){
    
  }

  produits: any;
  produitFind : any;

  ngOnInit(): void {
    this.loadProduits();
    this.produitService.newProduitReceive().subscribe(res => this.loadProduits());
    this.produitService.fielSearchObservation().subscribe(res =>{
      this.produitFind = res;
      this.loadProduit();
    });
  }
  
  loadProduits(){
    this.produitService.getProduits().subscribe((res) => {
      this.produits = res.produits;
    });
  }

  loadProduit(){
    this.produitService.searchProduit(this.produitFind).subscribe(res => {
      this.produits = res.produit;
    });
  }

  updateForm = new FormGroup({
    "nom": new FormControl('', Validators.required),
    "prix_unitaire": new FormControl('', Validators.min(0)),
    "quantite": new FormControl('', Validators.min(0))
  })

  update_produit(produit: any){
    this.matDialog.open(ProduitFormComponent).afterOpened().subscribe(res => {
      this.produitService.majProduitSend(produit);
    });
  }

  delete_produit(produit: any){
    this.produitService.deletProduit(produit).subscribe(res => {
      this.loadProduits();
    });
  }

}
