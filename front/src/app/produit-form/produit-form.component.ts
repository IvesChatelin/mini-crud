import { Component, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import { FormControl, FormGroup, NgModel, Validators } from '@angular/forms';
import { ProduitService } from '../service/produit.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-produit-form',
  templateUrl: './produit-form.component.html',
  styleUrls: ['./produit-form.component.css']
})
export class ProduitFormComponent implements OnInit{

  errorMsg: any;
  produit: any;
  nom: String = '';
  prix_unitaire: String = '';
  quantite: Number = 0;

  constructor(private produitService: ProduitService, private matDialog: MatDialog){}

  ngOnInit(): void {
    this.produitService.majProduitObservation().subscribe(res => {
      this.produit = res;
      this.nom = res.nom;
      this.prix_unitaire = res.prix_unitaire;
      this.quantite = res.quantite;
    });
  }

  produitForm = new FormGroup({
    "nom": new FormControl('',[Validators.required]),
    "prix_unitaire": new FormControl('', [Validators.min(0)]),
    "quantite": new FormControl('', [Validators.min(0), Validators.pattern("[1-9][0-9]*|0")])
  });
  
  envoyer(){
    if(this.produit){
      this.update();
    }else{
      if(this.produitForm.valid){
        this.produitService.searchProduit(this.produitForm.value).subscribe(res => {
          console.log(res);
          if((res.produit.length > 0) && (res.produit[0].nom = this.produitForm.value.nom)){
              this.produit = res.produit[0];
              this.update();
          }else{
            this.produitService.createProduit(this.produitForm.value).subscribe(res => {
              this.produitService.newProduitSend(res.newProduit);
            });
          }
        });
        this.close_form();
      }else{
        this.errorMsg = "formulaire non valide. verifiez!";
      }
    }
  }

  update(){
    if(this.produitForm.valid){
      this.produitService.updateProduit(this.produit,this.produitForm.value).subscribe(res => {
        this.produitService.newProduitSend(res);
      });
      this.close_form();
    }else{
      this.errorMsg = "formulaire non valide. verifiez!";
    }
  }

  close_form(){
    this.matDialog.closeAll();
  }

}
