import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  url_api = "https://zggeyrp7q6.execute-api.eu-north-1.amazonaws.com/beta/produit";

  constructor(private httpClient: HttpClient) { }

  //creation d'un sujet à observer
  private newProduit = new Subject<any>();
  private fielSearch = new Subject<any>();
  private majProduit = new Subject<any>();

  fielSearchValue(produit: any){
    this.fielSearch.next(produit);
  }

  newProduitSend(produit: any){
    this.newProduit.next(produit);
  }

  majProduitSend(produit: any){
    this.majProduit.next(produit);
  }

  //rendre le sujet observation
  newProduitReceive(): Observable<any>{
    return this.newProduit.asObservable();
  }
  fielSearchObservation(): Observable<any>{
    return this.fielSearch.asObservable();
  }
  majProduitObservation(): Observable<any>{
    return this.majProduit.asObservable();
  }

  getProduits(): Observable<any>{
    return this.httpClient.get(this.url_api);
  }

  createProduit(produit : any): Observable<any>{
    return this.httpClient.post(this.url_api,produit);
  }

  deletProduit(produit: any): Observable<any>{
    return this.httpClient.delete(this.url_api+"/"+produit.id);
  }

  updateProduit(produit: any, data: any): Observable<any>{
    return this.httpClient.put(this.url_api+"/"+produit.id,data);
  }

  searchProduit(produit: any): Observable<any>{
    return this.httpClient.get(this.url_api+"/"+produit.nom);
  }
}
