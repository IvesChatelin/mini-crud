import { Component, OnInit} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ProduitFormComponent } from './produit-form/produit-form.component';
import { ProduitService } from './service/produit.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'CRUD-Produit';

  isSearch: boolean = true;
  notFoundMsg: any;

  constructor(private matDialog: MatDialog, private produitService: ProduitService){}

  ngOnInit(): void {
    this.searchForm.valueChanges.subscribe(res => {
      if(this.searchForm.value.nom == ''){
        this.produitService.newProduitSend(this.searchForm.value);
      }else{
        this.produitService.fielSearchValue(this.searchForm.value);
      }
    })
  }

  poduit_form(){
    this.matDialog.open(ProduitFormComponent);
  }

  searchForm = new FormGroup({
    "nom": new FormControl('')
  });

  search(){
    this.produitService.searchProduit(this.searchForm.value).subscribe(res => {
      if(res.produit.length > 0){
        this.produitService.fielSearchValue(this.searchForm.value);
      }else{
        this.isSearch = false;
        this.notFoundMsg = "Aucun produit trouvé. verifiez les majuscules!";
      }
    })
  }
}
