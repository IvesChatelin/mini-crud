'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class produit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  produit.init({
    nom: DataTypes.STRING,
    prix_unitaire: DataTypes.FLOAT,
    quantite: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'produit',
  });
  return produit;
};