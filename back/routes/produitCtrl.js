var models = require('../models/');

module.exports = {
    add: function(req, res){
        var nom = req.body.nom;
        var prix_unitaire = req.body.prix_unitaire;
        var quantite = req.body.quantite;
        
        models.produit.create({
            nom: nom,
            prix_unitaire: prix_unitaire,
            quantite: quantite
        })
        .then(function(newProduit){
            return res.status(201).json({newProduit});
        })
        .catch(function(err){
            return res.status(500).json({'erreur': "produit non ajouté"});
        })
    },

    delete: function(req, res){

        models.produit.findOne({
            where: {id: req.params.id}
        })
        .then(function(produitfind){
            models.produit.destroy({
                where: {id: produitfind.id}
            })
            .then(function(){
                return res.status(200).json({'message': 'le produit '+produitfind.id+' a étè supprimer avec succès'});
            })
            .catch(function(err){
                return res.status(500).json({'erreur': "le produit n'a pas étè supprimé"})
            })
        })
        .catch(function(err){
            return res.status(204).json({'erreur': "le produit n'existe pas"});
        })

    },

    update: function(req, res){

        var nom = req.body.nom;
        var prix_unitaire = req.body.prix_unitaire;
        var quantite = req.body.quantite;

        models.produit.findOne({
            where: {id: req.params.id}
        })
        .then(function(produitfind){
            models.produit.update(
                {
                    nom: nom,
                    prix_unitaire: prix_unitaire,
                    quantite: quantite
                },
                {
                    where: {id: produitfind.id}
                }
            )
            .then(function(){
                return res.status(200).json({'message': "le produit a étè mis à jour avec succès"});
            })
            .catch(function(err){
                return res.status(500).json({'erreur': "le produit n'a pas étè mis à jour"});
            })
        })
        .catch(function(err){
            return res.status(204).json({'erreur': "le produit n'existe pas"});
        })
    },

    readAll: function(req, res){
        models.produit.findAll()
        .then(function(produits){
            return res.status(200).json({produits});
        })
        .catch(function(err){
            return res.status(500).json({err});
        })
    },

    readOneProduit: function(req, res){
        models.produit.findAll({
            where: {nom: req.params.nom}
        })
        .then(function(produit){
            return res.status(200).json({produit});
        })
        .catch(function(err){
            return res.status(204).json({'erreur': "le produit n'est pas disponible"});
        })
    }
}